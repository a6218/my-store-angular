# My Store [2022]

Created a My Store Single Page Application [Followed Tutorial].

## Source
[Getting started with Angular](https://angular.io/start)

## Project Description
- Angular version 14.0.0
- Navigate Cart Page
- Navigate to Details Page
- Navigate to Shipping Price Page
- ADD | GET | CLEAR Products

## Development server

- Run `ng serve` for a dev server
- Navigate to `http://localhost:4200/`
- The app will automatically reload if you change any of the source files

## Pages Overview

### My Store Page
![My Store Page](/src/assets/readme/main_page.png "My Store Page")

### Product Details Page
![Product Details Page](/src/assets/readme/product_details_page.png "Product Details Page")

### Cart Page
![Cart Page](/src/assets/readme/cart_page.png "Cart Page")

### Shipping Page
![Shipping Page](/src/assets/readme/shipping_page.png "Shipping Page")
