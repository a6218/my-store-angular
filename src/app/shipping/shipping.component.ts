import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { Observable } from 'rxjs';
import { Shipping } from '../shipping';

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.sass']
})
export class ShippingComponent implements OnInit {
  shippingCosts: Observable<Shipping[]> = this.cartService.getShippingPrice()

  constructor(private cartService: CartService) { }

  ngOnInit(): void { }

}
