import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { Product } from '../products';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.sass']
})
export class CartComponent implements OnInit {
  items: Product[] = this.cartService.getItems();
  hideErrors: boolean = true;

  checkoutForm = this.formBuilder.group({
    name: ['', Validators.required],
    address: ['', Validators.required],
  });

  constructor(
    private cartService: CartService, 
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void { }

  checkNameValid(): boolean {
    let nameValid: boolean = this.checkoutForm.value.name !== '';
    if (nameValid) {
      return true;
    }

    return false;
  }

  checkAddressValid(): boolean {
    let addressValid: boolean = this.checkoutForm.value.address !== '';
    if (addressValid) {
      return true;
    }

    return false;

  }

  onSubmit(): void {
    let valid:boolean = this.checkoutForm.valid;
    this.hideErrors = false;

    if (valid) {
      this.items = this.cartService.clearCart();
      console.warn('Your order has been submitted: ', this.checkoutForm.value);
      this.checkoutForm.reset;
    }
  }

}
